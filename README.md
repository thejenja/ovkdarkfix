# VK Night Fix 
Тема для OpenVK, исправляющая проблемы в VK Night с некоторыми нововедениями

Доступно в Stylus: https://userstyles.world/style/2420/vk-night-fix
## Примечания
- Применяйте с темой **VK Night**
## Как установить?
- Скачайте расширение [Stylus](https://github.com/openstyles/stylus)
- Зайдите на сайт OpenVK, нажмите на расширение
- Вставьте код с этого репозитория
- Выберите внизу 
  > Применить к: URL, начинающимся с https://openvk.su/
